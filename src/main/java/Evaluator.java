

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.streaming.StreamInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;


public class Evaluator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SparkConf conf = new SparkConf().setAppName("Language Evaluator");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JobConf jobConf = new JobConf();
		
		jobConf.set("stream.recordreader.class", "org.apache.hadoop.streaming.StreamXmlRecordReader");
		
		jobConf.set("stream.recordreader.begin", "<page>");
		
		jobConf.set("stream.recordreader.end", "</page>");
		
		org.apache.hadoop.mapred.FileInputFormat.addInputPaths(jobConf, "resources/wikidata.xml");
		
		JavaPairRDD wikiDocuments = sc.hadoopRDD(jobConf, StreamInputFormat.class, Text.class, Text.class);
		
	}

}
